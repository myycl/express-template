function errorHandler(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send(errorSendJson(401, 'invalid token...'));
  }
  else if (err.name === 'SqlError') {
    res.status(500).send(errorSendJson(500, err.message));
  }
  else if (err.name === 'ConflictError') {
    res.status(409).send(errorSendJson(409, err.message));
  }
  else if (err.name === 'VerifyError') {
    res.status(400).send(errorSendJson(400, err.message));
  }
  else {
    res.status(500).send(errorSendJson(500, 'Internal Server Error'));
  }
}

function errorSendJson(code, msg) {
  return ({
    code,
    msg: '🍅🍅🍅' + msg + '🍅🍅🍅'
  })
}

module.exports = errorHandler;