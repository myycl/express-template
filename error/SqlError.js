/**
 * 数据库错误
 */
class SqlError extends Error {
  constructor(message, err) {
    super(message);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, SqlError);
    }
    this.name = 'SqlError';
    this.inner = err;
  }
}

module.exports = SqlError
