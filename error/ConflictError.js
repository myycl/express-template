/**
 * 资源冲突错误
 */
class ConflictError extends Error {
  constructor(message, err) {
    super(message);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ConflictError);
    }
    this.name = 'ConflictError';
    this.inner = err;
  }
}

module.exports = ConflictError;