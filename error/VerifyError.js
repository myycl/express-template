/**
 * 校验数据错误
 */
class VerifyError extends Error {
  constructor(message, err) {
    super(message);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, VerifyError);
    }
    this.name = 'VerifyError';
    this.inner = err;
  }
}

module.exports = VerifyError;