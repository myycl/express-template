const express = require('express');
const bodyParser = require('body-parser');

const ColorsLog = require('./utils/console.log');
const createRouter = require('./router');
const errorHandler = require('./error/errorHandle');
const jwtAuth = require('./utils/jwt.auth')

const app = express();
// 鉴权
jwtAuth(app);
// 处理application/json内容格式的请求体
app.use(bodyParser.json());
// 挂载路由
createRouter(app);
// 错误处理
app.use(errorHandler);

app.listen(8848, () => {
  ColorsLog.success('Server is running on port 8848');
})