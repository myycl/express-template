const expressJWT = require('express-jwt');

const { unlessPath, algorithms } = require('../config/jwt.config');
const { JWT_SECRET } = require('../config/crypto.config');

const jwt = expressJWT.expressjwt;

module.exports = (app) => {
  // 鉴权路由设置
  app
    .use(jwt({ secret: JWT_SECRET, algorithms: algorithms })
      .unless({ path: unlessPath }))
}