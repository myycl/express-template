const colors = require('colors-console');
class ColorsLog {
  constructor() { }
  /**
   * 成功显示的打印
   * @param {string} msg 打印的字符串
   */
  success(msg) {
    console.log(colors(['black', 'greenBG'], '🍎  ' + msg));
  }
  /**
   * 失败显示的打印
   * @param {string} msg 打印的字符串
   */
  error(msg) {
    console.log(colors(['black', 'redBG'], '🍑  ' + msg));
  }
  /**
   * 警告显示的打印
   * @param {string} msg 打印的字符串
   */
  warn(msg) {
    console.log(colors(['black', 'yellowBG'], '⚠️  ' + msg));
  }
}

module.exports = new ColorsLog();