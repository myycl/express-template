class TypeCheck {
  constructor() { }
  /**
   * 校验数据是否为对象
   * @param {any} val 需要校验的值
   * @returns {boolean} 成功为true，失败为false
   */
  object(val) {
    return Object.prototype.toString.call(val) === '[object Object]'
  }

  /**
   * 校验数据是否为数组
   * @param {any} val 需要校验的值
   * @returns {boolean} 成功为true，失败为false
   */
  array(val) {
    return Array.isArray(val)
  }

  /**
   * 校验数据是否为字符串
   * @param {any} val 需要校验的值
   * @returns {boolean} 成功为true，失败为false
   */
  string(val) {
    return typeof val === 'string'
  }

  /**
   * 校验数据是否为数字
   * @param {any} val 需要校验的值
   * @returns {boolean} 成功为true，失败为false
   */
  number(val) {
    return typeof val === 'number'
  }

  /**
   * 校验数据是否为函数
   * @param {any} val 需要校验的值
   * @returns {boolean} 成功为true，失败为false
   */
  function(val) {
    return typeof val === 'function'
  }

  /**
   * 校验数据是否存在（不是null和undefined）
   * @param {any} val 需要校验的值
   * @returns {boolean} 成功为true，失败为false
   */
  required(val) {
    return val !== undefined && val !== null
  }
}

module.exports = new TypeCheck();