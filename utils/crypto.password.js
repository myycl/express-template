const crypto = require('crypto');
const { CRYPTO_KEY } = require("../config/crypto.config")

/**
 * 加密函数
 * @param {string} content 加密内容
 * @returns 加密后内容
 */
module.exports = (content, customCryptoKey) => {
  // 利用customCryptoKey，生成一个随机的key，作为最后生成加密内容的key
  const cryptoKey = crypto.createHmac("sha1", customCryptoKey + CRYPTO_KEY).update(content).digest("hex");
  return crypto.createHmac("sha1", cryptoKey).update(content).digest("hex");
}