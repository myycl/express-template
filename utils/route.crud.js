const FormatSql = require('../middleware/formatSql.middleware');
const CommonMiddleware = require('../middleware/common.middleware');
const CrudController = require('../controller/crud.controller');
const dataComplyWithRules = require('../middleware/verify/data.comply')

module.exports = (router, tableName, crud, crudRules) => {
  // 设置表名
  router.use(function (req, res, next) {
    req.tableName = tableName;
    next();
  })
  // 配置路由
  crudRules = crudRules || {};
  // 查询
  verifyAble(crud, 'find') && mountFind(router, crudRules.find);
  // 新增
  verifyAble(crud, 'add') && mountAdd(router, crudRules.add);
  // 更新数据
  verifyAble(crud, 'update') && mountUpdate(router, crudRules.update);
  // 删除数据
  verifyAble(crud, 'delete') && mountDelete(router, crudRules.delete);

}
/**
 * 检验是否包含对应能力
 * @param {boolean|Array} crud 路由基础能力标识
 * @param {string} ableString 能力字符串
 * @returns 
 */
function verifyAble(crud, ableString) {
  if (typeof crud.includes === 'boolean') {
    return true;
  }
  else if (Array.isArray(crud)) {
    return crud.includes(ableString);
  }
  else return false;
}

/**
 * 路由配置查询
 * @param {object} router 路由对象
 */
function mountFind(router, rules) {
  router.get(
    '/',
    dataComplyWithRules(rules),
    FormatSql.limit,
    FormatSql.getWhere,
    CommonMiddleware.getCount,
    CrudController.findData
  )
}
/**
 * 路由配置系新增
 * @param {object} router 路由对象
 */
function mountAdd(router, rules) {
  router.post(
    '/',
    dataComplyWithRules(rules),
    FormatSql.create,
    CrudController.addData
  )
}
/**
 * 路由配置更新
 * @param {object} router 路由对象
 */
function mountUpdate(router, rules) {
  router.put(
    '/',
    dataComplyWithRules(rules),
    FormatSql.notGetWhere,
    FormatSql.update,
    CrudController.updateData
  )
}
/**
 * 路由配置删除
 * @param {object} router 路由对象
 */
function mountDelete(router, rules) {
  router.delete(
    '/',
    dataComplyWithRules(rules),
    FormatSql.notGetWhere,
    CrudController.deleteData
  )
}