module.exports = {
  basicPath: "/api/v1/",
  routes: {
    test: {
      // 路由对应的接口地址
      path: "test",
      // 路由对应的表名
      tableName: "test",
      // 需要自动挂载的路由能力
      crud: ['find', 'update', 'add', 'delete'],
      // 路由数据校验规则
      crudRules: {
        find: [
          {
            /**
             * 当需要联合校验多个规则时
             * filed可以传空字符或者不传
             * 使用自定义校验函数校验，
             * 自定义校验函数的第二个参数为所有前端传递的数据
             * ⚠️：第一个参数可能为空，请在第二个参数内获取和判断
             *  */
            typeCheck: (_, data) => {
              if (Object.hasOwnProperty.call(data, 'pageNum') || Object.hasOwnProperty.call(data, 'pageSize')) {
                const pageNum = parseInt(data.pageNum);
                const pageSize = parseInt(data.pageSize);
                return (!Number.isNaN(pageNum) && !Number.isNaN(pageSize))
              } else {
                return false;
              }
            },
            errorMsg: '使用分页时,pageNum和pageSize使用时都必传且为数字'
          }
        ],
        add: [
          {
            /**
             * 使用自定义校验函数校验，
             * 自定义校验函数的第一个参数为当前字段数据值
             * 第二个参数为所有前端传递的数据，可以不用接收
             */
            filed: 'name',
            typeCheck: val => (typeof val === 'string' && val.length > 5),
            errorMsg: 'name必须为字符串,且长度大于5'
          }
        ],
        update: [
          {
            /**
             * 校验基本数据类型
             */
            filed: 'id',
            typeCheck: 'number',
            errorMsg: 'id必须为数字'
          },
          {
            filed: 'name',
            typeCheck: val => (typeof val === 'string' && val.length > 5),
            errorMsg: 'name必须为字符串,且长度大于5'
          }
        ],
        delete: [
          /**
           * 只需要校验是否传递该字段时，可直接写字段名称
           */
          'id'
        ]
      }
    }
  }
}