const accountReg = /^[a-zA-Z0-9]{8,}$/;
const passwordReg = /^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{8,}$/;

module.exports = {
  /**
   * 注册校验规则
   * @type {Array<string|object>}
   */
  register: [
    "nickname",
    {
      filed: "account",
      typeCheck: val => accountReg.test(val),
      errorMsg: "账号由a-zA-Z0-9组成的字符串,长度不能小于8位。"
    },
    {
      filed: "password",
      typeCheck: val => passwordReg.test(val),
      errorMsg: "密码由a-zA-Z0-9组成的字符串,长度不能小于8位,且必须包含字母和数字。"
    }
  ],
  /**
   * 登录校验规则
   * @type {Array<string|object>}
   */
  login: ["account", "password"]
}