const { basicPath } = require('../config/router.config');
const ColorsLog = require('../utils/console.log');
/**
 * unlessPath 路由白名单
 * @type {array<string>}
 */
const unless = ['auth/login', 'auth/register', { url: 'test', methods: ['GET'] }]

module.exports = {
  unlessPath: formatUnlessPath(unless),
  algorithms: ['HS256']
}
/**
 * 拼接unlessPath路由
 */
function formatUnlessPath(unlessPath) {
  return unlessPath.map((path, i) => {
    if (typeof path === 'string') {
      return basicPath + path;
    } else if (typeof path === 'object') {
      return {
        url: basicPath + path.url,
        methods: path.methods
      };
    } else {
      ColorsLog.warn(`unlessPath第${i + 1}个参数格式错误，格式只能时字符串或者对象`)
      return path + '';
    }
  })
};