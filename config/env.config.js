const fs = require('fs');
const dotEnv = require('dotenv');
const ColorsLog = require('../utils/console.log');

let fileExists = fs.existsSync('.env');

if (fileExists) {
  dotEnv.config({ path: '.env' });
} else {
  ColorsLog.error('根目录下.env文件未找到,.env需要提供USER,PASSWORD,HOST,PORT,DATABASE参数值');
}
