/**
 * 密码加密的加盐部分字符串
 * 在用户注册完成之后就**不能**改动了
 * 改动之后，原来账号密码注册的就不能登录了
 * @type {string}
 */
const CRYPTO_KEY = 'CRYPTO_KEY';

/**
 * jwt加密的加盐部分字符串
 * 可以随时改动，尽量不改动
 * @type {string}
 */
const JWT_SECRET = 'JWT_SECRET';

module.exports = {
  CRYPTO_KEY,
  JWT_SECRET
}