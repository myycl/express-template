const baseDao = require('../db/baseDao.db');
/**
 * @class CRUD操作
 */
class CrudController {
  constructor() { }
  /**
   * 查询数据
   */
  findData(req, res, next) {
    baseDao.findData(req).then(({ err, result }) => {
      if (!err) {
        res.send({
          code: 200,
          msg: '查询成功',
          data: {
            rows: result,
            total: req.total
          }
        });
      } else {
        next(err);
      }
    })
  }
  /**
   * 新增数据
   */
  addData(req, res, next) {
    baseDao.addData(req).then(({ err, result }) => {
      if (!err) {
        res.send({
          code: 200,
          msg: '添加成功',
          data: result.insertId
        });
      } else {
        next(err);
      }
    })
  }

  /**
   * 更新数据
   */
  updateData(req, res, next) {
    baseDao.updateData(req).then(({ err, result }) => {
      if (!err) {
        res.send({
          code: 200,
          msg: '更新成功'
        });
      } else {
        next(err);
      }
    })
  }

  /**
   * 删除数据
   */
  deleteData(req, res, next) {
    baseDao.deleteData(req).then(({ err, result }) => {
      if (!err) {
        res.send({
          code: 200,
          msg: '删除成功'
        });
      } else {
        next(err);
      }
    })
  }
}
/**
 * @type {CrudController} CRUD操作对象
 */
module.exports = new CrudController();