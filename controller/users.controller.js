const jwt = require('jsonwebtoken');

const genPws = require('../utils/crypto.password')
const baseDao = require('../db/baseDao.db');
const { JWT_SECRET } = require('../config/crypto.config')
/**
 * @class 用户信息操作--注册、登录
 */
class UsersController {
  constructor() { }

  // 注册
  register(req, res, next) {
    let pwdIndex = req.addSql.split(',').findIndex(item => (item.includes('password') || item.includes(' password')));
    let accountIndex = req.addSql.split(',').findIndex(item => (item.includes('account') || item.includes(' account')));
    req.sqlParams.forEach(element => {
      element[pwdIndex] = genPws(element[pwdIndex], element[accountIndex]);
    });
    baseDao.addData({
      tableName: 'users',
      sqlParams: req.sqlParams,
      addSql: req.addSql,
    }).then(({ err, result }) => {
      if (!err) {
        res.send({
          code: 200,
          msg: '注册成功',
          data: result.insertId
        });
      } else {
        next(err);
      }
    })
  }

  // 登录
  login(req, res, next) {
    // 登录成功后生成token
    let token = jwt.sign(
      Object.assign({}, req.user, {passwrod: ''}),
      JWT_SECRET,
      {
        expiresIn: 60 * 60 * 2 * 1000
      }
    );
    res.send({
      code: 200,
      msg: '登录成功',
      data: {
        token: token,
        user: req.user
      }
    })
  }
}
/**
 * @type {UsersController} 用户信息操作
 */
module.exports = new UsersController();