const mysql = require('mysql');

require('../config/env.config')

const poolOptions = {
  user: process.env.USER,
  password: process.env.PASSWORD,
  host: process.env.HOST,
  port: process.env.PORT,
  database: process.env.DATABASE,
}
/**
 * 数据库连接池
 * @type {mysql.Pool}
 */
let pool = null;

if (process.env.USER) {
  pool = mysql.createPool(poolOptions);
}
/**
 * @type {mysql.Pool} 数据库连接池
 */
module.exports = pool;