const pool = require('./pool');
const SqlError = require('../error/SqlError')

/**
 * 数据库事务处理函数
 * @param {Promise} createPromise 数据库操作集合构建的Promise
 * @returns {Promise} 返回一个Promise，resolve的结果为{err, results}，err为错误信息，results为执行的结果数组
 */
const execTransection = (createPromise) => {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) {
        // 获取数据库连接失败
        return resolve({ err });
      }
      connection.beginTransaction((err) => {
        if (err) {
          // 事务开启失败
          return resolve({ err: new SqlError('事务开启失败', err) });
        }
        createPromise().then(({ err, result }) => {
          if (err) {
            // 失败时执行回滚
            connection.rollback(() => {
              connection.release();
              return resolve({ err: new SqlError('事务执行失败', err) })
            });
          } else {
            // 成功则提交事务
            connection.commit((err) => {
              if (err) {
                // 事务提交失败执行回滚
                connection.rollback(() => {
                  connection.release();
                  return resolve({ err: new SqlError('事务提交失败', err) });
                });
              }
              return resolve({ err, result });
            })
          }
        })
      })
    })
  })
}

module.exports = execTransection;