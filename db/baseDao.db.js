const pool = require('./pool')
const SqlError = require('../error/SqlError');
/**
 * @class 增删改查的整合对象
 */
class BaseDao {
  constructor() { }

  /**
   * 新增数据的统一函数
   * @param {object} options 新增数据配置
   * @param {string} options.tableName 新增数据的表
   * @param {string} options.addSql 新增数据的字段拼接字符串
   * @param {array<array>} options.sqlParams 新增数据的字段拼接字符串，二维数组，内部数组为单条数据的值
   * @returns Promise.resolve
   */
  addData(options) {
    return new Promise((resolve, reject) => {
      let addSql = `INSERT INTO ${options.tableName}(${options.addSql}) VALUES ?;`;
      pool.query(addSql, [options.sqlParams], (err, result) => {
        if (err) {
          resolve({ err: new SqlError(err.sqlMessage, err) });
        } else {
          resolve({ err, result });
        }
      })
    })
  }
  /**
   * 查询数据的统一函数
   * @param {object} options 获取数据的配置
   * @param {string} options.sqlWhere 获取数据的查询条件
   * @param {string} options.tableName 获取数据的表
   * @param {string} options.sqlLimit 获取数据的分页sql
   */
  findData(options) {
    return new Promise((resolve, reject) => {
      let getSql = `SELECT * FROM ${options.tableName} ${options.sqlWhere} ${options.sqlLimit || ''};`;
      pool.query(getSql, options.sqlWhereParams, (err, result) => {
        if (err) {
          resolve({ err: new SqlError(err.sqlMessage, err) });
        } else {
          resolve({ err, result });
        }
      })
    })
  }/**
   * 查询数据总数的统一函数
   * @param {object} options 获取数据的配置
   * @param {string} options.tableName 获取数据的表
   * @param {string} options.sqlWhere 获取数据的查询条件
   * @param {array} options.sqlWhereParams 获取数据的查询条件的值
   */
  findDataCount(options) {
    return new Promise((resolve, reject) => {
      let getSql = `SELECT count(0) as total FROM ${options.tableName} ${options.sqlWhere};`;
      pool.query(getSql, options.sqlWhereParams, (err, result) => {
        if (err) {
          resolve({ err: new SqlError(err.sqlMessage, err) });
        } else {
          resolve({ err, result });
        }
      })
    })
  }
  /**
   * 修改数据的统一函数
   * @param {object} options 修改数据配置
   * @param {string} options.tableName 修改数据的表
   * @param {string} options.sqlWhere 修改数据的查询条件
   * @param {array} options.sqlWhereParams 修改数据的查询条件的值
   * @param {string} options.updateSql 修改数据的字段拼接字符串
   * @param {array} options.sqlParams 修改数据的字段拼接字符串，二维数组，内部数组为单条数据的值
   * @returns Promise.resolve
   */
  updateData(options) {
    return new Promise((resolve, reject) => {
      let updateSql = `UPDATE ${options.tableName} ${options.updateSql} ${options.sqlWhere};`;
      pool.query(updateSql, [...options.sqlParams, ...options.sqlWhereParams], (err, result) => {
        if (err) {
          resolve({ err: new SqlError(err.sqlMessage, err) });
        } else {
          resolve({ err, result });
        }
      })
    })
  }

  /**
   * 删除数据的统一函数
   * @param {object} options 删除数据配置
   * @param {string} options.tableName 删除数据的表
   * @param {string} options.sqlWhere 删除数据的查询条件
   * @param {array} options.sqlWhereParams 获取数据的查询条件的值
   * @returns Promise.resolve
   */
  deleteData(options) {
    return new Promise((resolve, reject) => {
      let deleteSql = `DELETE FROM ${options.tableName} ${options.sqlWhere};`;
      pool.query(deleteSql, options.sqlWhereParams, (err, result) => {
        if (err) {
          resolve({ err: new SqlError(err.sqlMessage, err) });
        } else {
          resolve({ err, result });
        }
      })
    })
  }
}
/**
 * @type {BaseDao} 数据库增删改查操作的对象
 */
module.exports = new BaseDao();