const execTransection = require('../../db/transaction.db');
const baseDao = require('../../db/baseDao.db');

module.exports = (router) => {
  // 使用事务修改两张表的数据
  router.post('/', updateName)
}

/**
 * 更新操作函数
 */
const updateName = (req, res, next) => {
  execTransection(testTransection()).then(({ err, result }) => {
    console.log(err, result);
    if (err) {
      return next(err)
    } else {
      res.send({
        code: 200,
        msg: "Transaction Success",
      })
    }
  })
}

/**
 * 执行事务的sql操作集合
 * @returns {Promise<{err: (null|object), result?: string}>}
 */
const testTransection = () => {
  return () => {
    return new Promise(async (resolve, reject) => {
      let toName = '张三';
      // users表id为1的nickname修改为张三
      await baseDao.updateData({
        tableName: "users",
        sqlWhere: "WHERE id = ?",
        updateSql: "SET nickname = ?",
        sqlParams: [toName],
        sqlWhereParams: [1]
      }).then(({ err }) => {
        if (err) {
          console.log('第一个执行失败');
          return resolve({ err })
        }
        console.log('第一个执行成功');
      })

      // test表id为1的name修改为张三
      baseDao.updateData({
        tableName: "test",
        sqlWhere: "WHERE id = ?",
        updateSql: "SET name = ?",
        sqlParams: [toName],
        sqlWhereParams: [1]
      }).then(({ err }) => {
        if (err) {
          console.log('第二个执行失败');
          return resolve({ err })
        }
        console.log('第二个执行成功');
        return resolve({ err: null, result: 'Transaction Success' })
      })
    })
  }
}