const fs = require('fs');

const express = require('express');

const routerConfig = require('../../config/router.config');

module.exports = (app) => {
  const files = fs.readdirSync(__dirname);

  files.forEach((file) => {
    if (file.endsWith('.router.js')) {
      // 创建路由对象
      const router = express.Router();
      // 导入路由挂载方法
      const mountRouter = require(`./${file}`);
      // 挂载路由
      mountRouter(router);
      // 路由路径字符
      const path = file.replace('.router.js', '');
      // 注册路由
      app.use(routerConfig.basicPath + path, router);
    }
  })
}