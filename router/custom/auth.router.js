const FormatSql = require('../../middleware/formatSql.middleware');
const UsersMiddleware = require('../../middleware/verify/users.middleware');
const UsersController = require('../../controller/users.controller');
const dataComplyWithRules = require('../../middleware/verify/data.comply');
const { register, login } = require('../../config/rules.config');

module.exports = function (router) {
  // 注册
  router.post(
    '/register',
    dataComplyWithRules(register),
    FormatSql.create,
    UsersMiddleware.verifyExistByAccount,
    UsersController.register
  )

  // 登录
  router.post(
    '/login',
    dataComplyWithRules(login),
    UsersMiddleware.verifyExistUserByAccountPassword,
    UsersController.login
  )
}