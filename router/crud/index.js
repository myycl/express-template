const express = require('express');

const routerConfig = require('../../config/router.config');
const autoBasicCrud = require('../../utils/route.crud');
const extraRoute = require('../extra');

module.exports = (app) => {
  // 配置自动增删改查路由
  const routes = routerConfig.routes;
  // 循环路由配置，根据规则自动挂载增删改查能力
  for (const routeKey in routes) {
    if (Object.hasOwnProperty.call(routes, routeKey)) {
      const router = express.Router();
      const routeConfig = routes[routeKey];
      // 路由对应表名
      const tableName = routeConfig.tableName;
      // 路由路径
      const routerPath = routeConfig.path;
      // 实现功能：1.添加表名，2.处理增删改查
      routeConfig.crud && autoBasicCrud(router, tableName, routeConfig.crud, routeConfig.crudRules);
      // 检查是否存在额外路由，有就添加
      (typeof extraRoute[routerPath] == 'function') && extraRoute[routerPath](router);
      // 注册路由
      app.use(routerConfig.basicPath + routerPath, router);
    }
  }
}