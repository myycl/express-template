const createCustomRoute = require('./custom')
const createCrudRoute = require('./crud')

module.exports = (app) => {
  // 配置自动增删改查路由
  createCrudRoute(app);
  // 自定义路由添加
  createCustomRoute(app);
}