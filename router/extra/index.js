const fs = require('fs');

const files = fs.readdirSync(__dirname);
let extraRoute = {};
files.forEach(file => {
  if (file.endsWith('.router.js')) {
    const extraFn = require(__dirname + '/' + file);
    let routerPath = file.replace('.router.js', '');
    (typeof extraFn === 'function') && (extraRoute[routerPath] = extraFn)
  }
})

module.exports = extraRoute;