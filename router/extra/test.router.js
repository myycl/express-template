/**
 * 
 * @param {object} router 需要挂载的router对象
 */
function testRouter(router) {
  // 额外路由
  router.get('/list', (req, res) => {
    res.status(200).send('成功')
  })
}

module.exports = testRouter;