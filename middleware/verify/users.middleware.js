const baseDao = require('../../db/baseDao.db');
const genPws = require('../../utils/crypto.password');
const ConflictError = require('../../error/ConflictError');
const VerifyError = require('../../error/VerifyError');

class UsersMiddleware {
  constructor() { }
  /**
   * 验证账号是否存在
   */
  verifyExistByAccount(req, res, next) {
    baseDao.findData({
      tableName: 'users',
      sqlWhere: 'WHERE account = ?',
      sqlWhereParams: [req.body.add.account],
    }).then(({ err, result }) => {
      if (!err) {
        if (result.length > 0) {
          next(new ConflictError('该账号已存在'));
        } else {
          next();
        }
      } else {
        next(err);
      }
    })
  }

  /**
   * 根据账号密码查询用户是否存在
   * */
  verifyExistUserByAccountPassword(req, res, next) {
    let account = req.body.where.account;
    let password = req.body.where.password;
    baseDao.findData({
      tableName: 'users',
      sqlWhere: 'WHERE account = ? AND password = ?',
      sqlWhereParams: [account, genPws(password, account)],
    }).then(({ err, result }) => {
      if(!err) {
        if (result.length > 0) {
          req.user = Object.assign({}, result[0], {password: ''});
          next();
        } else {
          next(new VerifyError('账号或密码错误'));
        }
      } else {
        next(new VerifyError('账号或密码错误'));
      }
    })
  }
}

module.exports = new UsersMiddleware();