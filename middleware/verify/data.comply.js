const ColorsLog = require('../../utils/console.log');
const VerifyError = require('../../error/VerifyError');
const TypeCheck = require('../../utils/type.check');

/**
 * 数据遵守规则中间件函数
 * 需要校验数据为 **req** 请求对象的query、body.where、body.add、body.update 中的数据
 * @param {Array<string|object>} rules 需要遵守的规则数组
 * @returns {Function} 中间件函数，中间件在校验通过会执行next，校验失败会执行next(err)
 */
const dataComplyWithRules = (rules) => {
  return (req, res, next) => {
    // 存在规则数组
    if (TypeCheck.array(rules) && rules.length) {
      // 所有需要校验的数据
      const data = Object.assign(
        {},
        req.query || {},
        req.body.where || {},
        getAddData(req.body.add) || {},
        req.body.update || {}
      );
      // 循环规则
      for (let i = 0; i < rules.length; i++) {
        let rule = rules[i];
        // 单项规则是对象时
        if (TypeCheck.object(rule)) {
          let typeCheck = rule.typeCheck;
          let filed = rule.filed;
          // 校验规则时函数时
          if (TypeCheck.function(typeCheck)) {
            let item = !filed ? null : data[filed];
            if (!typeCheck(item, data)) {
              return errorHandle('', next, `${rule.errorMsg}`);
            }
          }
          // 校验规则是字符串，表示校验当前字段的数据类型
          else if (TypeCheck.string(typeCheck)) {
            if (!TypeCheck[typeCheck](data[filed])) {
              let errorMsg = rule.errorMsg || `${typeCheck}类型校验失败，应该为${typeCheck}`;
              return errorHandle('', next, errorMsg);
            }
          }
        }
        // 单项规则是字符串时，字符串为字段名，表示当前字段是必须的
        else if (TypeCheck.string(rule)) {
          if (!TypeCheck.required(data[rule])) {
            return errorHandle('', next, `${rule}字段是必须的`);
          }
        }
        // 其他，表示规则配置异常
        else {
          return errorHandle(`rules第${i}数据不符合验证规则，应该为字符串或者对象`, next);
        }
      }
    }
    // 不存在数据规则或者校验通过，执行next表示校验通过
    next();
  }
}
/**
 * 合理化add数据
 * 🍇因为add数据可能为数组，也可能为对象，所以需要判断
 * 🍈当是数组时，只校验了第一组数据
 */
function getAddData(add) {
  if (TypeCheck.array(add) && add.length) {
    return add[0]
  } else if (TypeCheck.object(add)) {
    return add
  } else {
    return {}
  }
}

function errorHandle(msg, next, sendMsg = '数据不符合规则') {
  msg && ColorsLog.error(msg);
  return next(new VerifyError(sendMsg))
}


module.exports = dataComplyWithRules;