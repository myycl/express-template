const baseDao = require('../db/baseDao.db');

class CommonMiddleware {
  constructor() {
  }

  /**
   * 查询数据总条数
   */
  getCount(req, res, next) {
    baseDao.findDataCount(req).then(({ err, result }) => {
      if (!err) {
        req.total = result[0].total;
      } else {
        next(err);
      }
      next()
    })
  }
}

module.exports = new CommonMiddleware();