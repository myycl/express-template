/**
 * @class 整理sql的where条件、limit条件、新增数据、更新数据对象
 */
class FormatSql {
  constructor() { }
  /**
   * 整理limit字符串
   * 🥭query 参数格式：{pageNum: 1, pageSize: 10}
   */
  limit(req, res, next) {
    let sqlLimit = '';
    let query = req.query;
    const needLimit = Object.hasOwnProperty.call(query, 'pageNum') && Object.hasOwnProperty.call(query, 'pageSize');
    if (needLimit) {
      const pageNum = parseInt(query.pageNum);
      const pageSize = parseInt(query.pageSize);
      sqlLimit = `LIMIT ${(pageNum - 1) * pageSize}, ${pageSize}`;
    }
    req.sqlLimit = sqlLimit;
    delete req.query.pageNum;
    delete req.query.pageSize;
    next();
  }

  /**
   * 整理get请求的where条件字符串
   * 🥭query 参数格式：{name: '张三', age: 18}
   */
  getWhere(req, res, next) {
    let sqlWhere = '';
    let sqlWhereParams = [];
    let whereObj = req.query;
    // 整理查询sql语句 
    for (const key in whereObj) {
      if (Object.hasOwnProperty.call(whereObj, key)) {
        const element = whereObj[key];
        if (element) {
          let sqlWhereLen = sqlWhere.length;
          sqlWhere += `${sqlWhereLen ? ' AND' : 'WHERE'} ${key} = ?`;
          sqlWhereParams.push(element);
        }
      }
    }
    req.sqlWhere = sqlWhere;
    req.sqlWhereParams = sqlWhereParams;
    next();
  }

  /**
   * 整理非get请求的where条件字符串
   * 🥭body 参数格式：{where: {name: '张三', age: 18}}
   */
  notGetWhere(req, res, next) {
    let sqlWhere = '';
    let sqlWhereParams = [];
    let whereObj = req.body.where;
    // 整理查询sql语句 
    for (const key in whereObj) {
      if (Object.hasOwnProperty.call(whereObj, key)) {
        const element = whereObj[key];
        if (element) {
          let sqlWhereLen = sqlWhere.length;
          sqlWhere += `${sqlWhereLen ? ' AND' : 'WHERE'} ${key} = ?`;
          sqlWhereParams.push(element);
        }
      }
    }
    req.sqlWhere = sqlWhere;
    req.sqlWhereParams = sqlWhereParams;
    next();
  }

  /**
   * 新增请求体整理
   * 🥭body 参数格式：{add: {name: '张三', age: 18}}
   */
  create(req, res, next) {
    const addData = req.body.add;
    let addSql = '';
    let sqlParams = [];
    let isObject = Object.prototype.toString.call(addData) === '[object Object]';
    if (isObject) {
      let params = [];
      for (const key in addData) {
        if (Object.hasOwnProperty.call(addData, key)) {
          const element = addData[key];
          if (element === 0 || element) {
            addSql += `${addSql ? ', ' : ''}${key}`;
            params.push(element);
          }
        }
      }
      sqlParams.push(params);
    } else if (Array.isArray(addData) && addData.length) {
      let one = addData[0];
      let oneKeys = Object.keys(one);
      addSql = oneKeys.join(',');
      addData.forEach(item => {
        let params = [];
        oneKeys.forEach(key => {
          params.push(item[key]);
        })
        sqlParams.push(params);
      })
    }
    req.addSql = addSql;
    req.sqlParams = sqlParams;
    next();
  }

  /**
   * 更新请求体整理
   * 🥭body 参数格式：{update: {name: '张三', age: 18}}
   */
  update(req, res, next) {
    const updateData = req.body.update;
    let updateSql = '';
    let params = [];
    for (const key in updateData) {
      if (Object.hasOwnProperty.call(updateData, key)) {
        const element = updateData[key];
        if (element === 0 || element) {
          let updateSqlLen = updateSql.length;
          updateSql += `${updateSqlLen ? ', ' : 'SET '} ${key} = ?`;
          params.push(element);
        }
      }
    }
    req.updateSql = updateSql;
    req.sqlParams = params;
    next();
  }

}

module.exports = new FormatSql();